## Objective

- When checking yesterday's assignments during the Code Review, I got a better understanding of React's rendering mechanism.
- Today's course introduction or practice includes the following aspects
  - Roles in agile development, including BA, QA, UX, Dev, Tech Lead, etc
  - Activities in agile development, Standup, BA Clarify Requirement, IPM, Kick off, Desk check, Code Review, Retrospective, etc. In addition, there are Elevator Speech, User Journey, and User Story Card showcases.

## Reflective

- After today's lesson, there is a better understanding and experience of the process of agile development.

## Interpretive

- In the React component, if you maintain a variable of normal type via useState and send a direct request to get the data to update the state, the page will only be rendered once. But if the maintained variable is of type object, it will cause duplicate rendering. Because in the React mechanism, if you maintain a variable of ordinary type, compare the real value of the variable, and if it is an object type, it will compare whether the pointer to the variable is the same, although the value inside is the same. So you should use useEffect to send a request only once when the page is loaded.

## Decisional

- Although the teacher introduced many agile development activities today, the specific content of these activities is still not clear enough, so I should take the time to digest today's course content or go online for more specific sharing.